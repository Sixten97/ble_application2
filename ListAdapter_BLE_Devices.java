package com.example.ble_application01;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class ListAdapter_BLE_Devices extends ArrayAdapter<BLE_Device> {

    Activity activity;
    int layoutResourceID;
    ArrayList<BLE_Device> devices;

    public ListAdapter_BLE_Devices(Activity activity, int resource, ArrayList<BLE_Device> objects) {
        super(activity.getApplicationContext(), resource, objects);
        this.activity = activity;
        layoutResourceID = resource;
        devices = objects;
    }

   // @Override
    /*public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceID, parent, false);
        }

        BLE_Device device = devices.get(position);
        String name = device.getName();
        String address = device.getAddress();
        int rssi = device.getRSSI();

        TextView device_name = (TextView) convertView.findViewById(R.id.device_Name);
        if (name != null && name.length() > 0) {
            device_name.setText(device.getName());
        }
        else {
            device_name.setText("No Name");
        }

        TextView device_rssi = (TextView) convertView.findViewById(R.id.device_rssi);
        device_rssi.setText("RSSI: " + Integer.toString(rssi));

        TextView device_macaddr = (TextView) convertView.findViewById(R.id.device_macaddr);
        if (address != null && address.length() > 0) {
            device_macaddr.setText(device.getAddress());
        }
        else {
            device_macaddr.setText("No Address");
        }

        return convertView;
    }*/
}