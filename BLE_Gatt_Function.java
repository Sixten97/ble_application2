package com.example.ble_application01;

import android.app.Service;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.UUID;

public class BLE_Gatt_Function extends Service{
    private BluetoothGatt mBleGatt;
    private BLE_Device ble_device;
    private MainActivity ma;

    private boolean isConnected = false;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final UUID SERVICE_UUID = UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
    private static final UUID READ_WRITE_UUID = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8");

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public BLE_Gatt_Function(MainActivity mainActivity){
        ma = mainActivity;
    }

    private final BluetoothGattCallback mBleGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if(status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED){
                ma.status.setText("Connected");
                gatt.discoverServices();
            }
            else {
                ma.status.setText("Faild");
                gatt.disconnect();
            }
            super.onConnectionStateChange(gatt, status, newState);

        }
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status){
            mBleGatt = gatt;
            BluetoothGattCharacteristic mCharacteristic = gatt.getService(SERVICE_UUID).getCharacteristic(READ_WRITE_UUID);
            gatt.readCharacteristic(mCharacteristic);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if(status == BluetoothGatt.GATT_SUCCESS){
                final byte[] data = characteristic.getValue();
                String message = new String(data,0,data.length); //Turns the data into a string
                ma.device_data.setText(message);                       //Writes the string in the data field
            }
            gatt.setCharacteristicNotification(characteristic, true); //Tells the app to automatically update when new information is available
        }

       @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if(status == BluetoothGatt.GATT_SUCCESS){
                ma.status.setText("Success");
            }
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
        }
    };

    public boolean connect(BLE_Device device) {
        mBleGatt = device.getBluetoothDevice().connectGatt(this, true, mBleGattCallback);
        return true;
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBleGatt == null) {
            return;
        }
        mBleGatt.readCharacteristic(characteristic);
    }

    public void writeCharacteristics(String input){
        try
        {
            BluetoothGattCharacteristic mCharacteristic = mBleGatt.getService(SERVICE_UUID).getCharacteristic(READ_WRITE_UUID);
            mCharacteristic.setValue(input);
            mBleGatt.writeCharacteristic(mCharacteristic);
        }catch (IllegalStateException | NullPointerException e)
        {
            ma.status.setText("Faild");
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
