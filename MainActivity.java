package com.example.ble_application01;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.security.Provider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


    private HashMap<String, BLE_Device> mBLEDeviceHashMap;
    private ArrayList<BLE_Device> mBLEDeviceArrayList;
    private ListAdapter_BLE_Devices adapter;
    private BLE_Gatt_Function mBleGatt;
    //private BluetoothGattCharacteristic mGattCharacteristics;
    //private BluetoothGattService gattService;

    private Button listButton, scanButton,sendButton, onButton, offButton;
    private TextView device_name,device_macaddr;
    public TextView status,device_data,device_rssi;
    private ListView ble_devices;
    private EditText writeMsg;

    private BluetoothAdapter btAdapter;
    //private BluetoothDevice[] btDeviceArray;
    private BLE_Scanner mBLEScanner;

    public static final int REQUEST_ENABLE_BT = 1;
    static final int STATE_LISTENING = 2;
    static final int STATE_CONNECTING = 3;
    static final int STATE_CONNECTED = 4;
    static final int STATE_CONNECTION_FAILED = 5;
    static final int STATE_MESSAGE_RECEIVED = 6;

    //private static final String APP_NAME = "BLE";
    //private static final UUID MY_UUID = UUID.fromString("d1c4ad0c-2101-11eb-adc1-0242ac120002");

    private String latestMacAddr = null; //The MacAddress of the latest pressed device in the list
    private String receiveBuffer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Checks if the device supports BLE
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            Utils.toast(getApplicationContext(), "Device does not support BLE");
            finish();
        }

        mBLEDeviceHashMap = new HashMap<>();
        mBLEDeviceArrayList = new ArrayList<>();
        adapter = new ListAdapter_BLE_Devices(this, R.layout.activity_main, mBLEDeviceArrayList);
        mBleGatt = new BLE_Gatt_Function(this);

        mBLEScanner = new BLE_Scanner(this, 7500, -75);
        findViewByIdes();
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        implementListeners();

        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                switch (msg.what){
                    case STATE_LISTENING:
                        status.setText("Listening");
                        status.setBackgroundColor(Color.YELLOW);
                        break;
                    case STATE_CONNECTING:
                        status.setText("Connecting");
                        status.setBackgroundColor(Color.BLUE);
                        break;
                    case STATE_CONNECTED:
                        status.setText("Connected");
                        //msgBox.setText("Ready to send or receive");
                        status.setBackgroundColor(Color.GREEN);
                        break;
                    case STATE_CONNECTION_FAILED:
                        status.setText("Connection failed");
                        status.setBackgroundColor(Color.RED);
                        break;
                    case STATE_MESSAGE_RECEIVED:
                        byte[] readBuffer = (byte[]) msg.obj;
                        String tempMsg = new String(readBuffer,0, msg.arg1);
                        //msgBox.setText(tempMsg);
                        break;
                }
                return true;
            }
        }); //Use later
    }



    private void implementListeners() {

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyDeviceList();
                mBLEDeviceArrayList.clear();
                Utils.toast(getApplicationContext(), "Scan Button Pressed");
                if (!mBLEScanner.isScanning()) {
                    startScan();
                }
                else {
                    stopScan();
                    mBLEScanner.stopProximityScan();
                }
            }
        });
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        ble_devices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!mBLEScanner.isScanning()){
                    BLE_Device mBleDevice = mBLEDeviceArrayList.get(position);
                    latestMacAddr = mBleDevice.getAddress();
                    displayBLEDevice(mBleDevice);
                    mBleGatt.connect(mBleDevice);
                }
            }
        });
        onButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //String input = "Hello ble";
            //mBleGatt.writeCharacteristics(input);
                if(latestMacAddr != null){
                    mBLEScanner.startProximityScan(latestMacAddr);
                }
            }
        });
        offButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBLEScanner.stopProximityScan();
            }
        });

    }

    public void emptyDeviceList(){
        int length = mBLEDeviceArrayList.size();
        if(length == 0){
            return;
        }
        else{
            String[] deviceList = new String[mBLEDeviceArrayList.size()];
            for(int i = 0; i < length; i++){
                deviceList[i] = "";
            }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,deviceList);
            ble_devices.setAdapter(arrayAdapter);
        }

    }
    public void showListOfDevices(){
        if(mBLEDeviceArrayList.size() == 0){
            Utils.toast(getApplicationContext(), "No devices found");
        }
        else {
            int index = 0;
            String[] deviceList = new String[mBLEDeviceArrayList.size()];

            for(BLE_Device device: mBLEDeviceArrayList){
                if(device.getName() == null){
                    deviceList[index] = "No name";
                }
                else{
                    deviceList[index] = device.getName();
                }
                index++;
            }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,deviceList);
            ble_devices.setAdapter(arrayAdapter);
        }
    }

    private void findViewByIdes() {
        scanButton = (Button) findViewById(R.id.scanButton);
        sendButton = (Button) findViewById(R.id.sendButton);
        listButton = (Button) findViewById(R.id.listButton);
        onButton = (Button) findViewById(R.id.onButton);
        offButton = (Button) findViewById(R.id.offButton);
        ble_devices = (ListView) findViewById(R.id.listView);
        status = (TextView) findViewById(R.id.statusView);
        device_name = (TextView) findViewById(R.id.device_Name);
        device_rssi = (TextView) findViewById(R.id.device_rssi);
        device_macaddr = (TextView) findViewById(R.id.device_macaddr);
        device_data = (TextView) findViewById(R.id.device_Data);
        writeMsg = (EditText) findViewById(R.id.editText);
    }

    public void addDevice(BluetoothDevice device, int new_rssi) {
        String address = device.getAddress();
        if(!mBLEDeviceHashMap.containsKey(address)){
            BLE_Device ble_device = new BLE_Device(device);
            ble_device.setRSSI(new_rssi);

            mBLEDeviceHashMap.put(address, ble_device);
            mBLEDeviceArrayList.add(ble_device);
        }
        else{
            mBLEDeviceHashMap.get(address).setRSSI(new_rssi);
        }
        adapter.notifyDataSetChanged();

    }

    public void startScan(){
        status.setText("Scanning");
        mBLEDeviceArrayList.clear();
        mBLEDeviceHashMap.clear();
        mBLEScanner.start();
    }
    public void stopScan() {
        status.setText("Scan Again");
        mBLEScanner.stop();
    }

    private final BroadcastReceiver mGattReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(BLE_Gatt_Function.ACTION_DATA_AVAILABLE.equals(action)){
                receiveBuffer += intent.getStringExtra(BLE_Gatt_Function.EXTRA_DATA);
                if(receiveBuffer.contains("/n")){ // /n means end of line
                    receiveBuffer = receiveBuffer.substring(0, receiveBuffer.length() - 1);
                    setReceivedData();
                    receiveBuffer = "";
                }
            }
        }
    };

    public void setReceivedData(){
        device_data.setText(receiveBuffer);
    }
    public void displayBLEDevice(BLE_Device device){
        String name = device.getName();
        String address = device.getAddress();
        int rssi = device.getRSSI();

        if (name != null && name.length() > 0) {
            device_name.setText(device.getName());
        }
        else {
            device_name.setText("No Name");
        }

        device_rssi.setText("RSSI: " + Integer.toString(rssi));

        if (address != null && address.length() > 0) {
            device_macaddr.setText(device.getAddress());
        }
        else {
            device_macaddr.setText("No Address");
        }
    }

    public void displayGattService(){

    }
}