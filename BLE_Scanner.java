package com.example.ble_application01;

import android.bluetooth.BluetoothAdapter;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;

public class BLE_Scanner {

    private MainActivity ma;
    private BluetoothAdapter bluetoothAdapter;
    private boolean mScanning;
    private BLE_Device mBle_device;
    //private BluetoothGatt gatt;

    private long scanPeriod;
    private int signalStrength;
    private String mMacAddr;

    private Handler mHandler;
    public BLE_Scanner(MainActivity mainActivity, long scanPeriod, int signalStrength){
        ma = mainActivity;
        mHandler = new Handler();
        this.scanPeriod = scanPeriod;
        this.signalStrength = signalStrength;

        final BluetoothManager bluetoothManager = (BluetoothManager) ma.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
    }

    public boolean isScanning(){
        return mScanning;
    }

    public void start(){
        if(!Utils.checkBluetooth(bluetoothAdapter)){
            Utils.requestUserBluetooth(ma);
            ma.stopScan();
        }
        else{
            scanLeDevice(true);
        }
    }

    public void stop(){
        scanLeDevice(false);
        ma.showListOfDevices();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable && !mScanning) {
            //Utils.toast(ma.getApplication(), "Starting BLE scan");

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Utils.toast(ma.getApplicationContext(), "Stopping BLE scan");
                    mScanning = false;
                    bluetoothAdapter.stopLeScan(mLeScanCallback);
                    ma.stopScan();
                }
            }, scanPeriod);
            mScanning = true;
            bluetoothAdapter.startLeScan(mLeScanCallback);

        } else {
            mScanning = false;
            bluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            //Utils.toast(ma.getApplication(), "on LeScan");
            final int new_rssi = rssi;
            if(device.getAddress().equals(mMacAddr)){
                String mRssi = Integer.toString(rssi);
                ma.device_rssi.setText("RSSI: " + mRssi);
            }
            if(rssi > signalStrength && mMacAddr == null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        ma.addDevice(device, rssi);
                    }
                });
            }
        }
    };

    public void startProximityScan(String macAddress){
        mScanning = true;
        mMacAddr = macAddress;
        ma.status.setText("Proximity scan");
        bluetoothAdapter.startLeScan(mLeScanCallback);

    }
    public void stopProximityScan(){
        if(mScanning){
            mScanning = false;
            mMacAddr = null;
            bluetoothAdapter.stopLeScan(mLeScanCallback);
            ma.status.setText("Scan again");
        }
    }
    private BluetoothAdapter.LeScanCallback mProximityScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            if(device.getAddress().equals(mMacAddr)){
                String mRssi = Integer.toString(rssi);
                ma.device_rssi.setText("RSSI: " + mRssi);
            }
        }
    };

}
